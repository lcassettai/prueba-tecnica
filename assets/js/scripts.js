function confirmarEliminacion(evento){
    if(!confirm("¿Está seguro? esta acción no se puede deshacer")){
        evento.preventDefault()
    }
}

document.addEventListener("DOMContentLoaded", function() {
    document
        .querySelectorAll(".eliminar")
        .forEach(elemento => elemento.addEventListener("click", confirmarEliminacion));
});
