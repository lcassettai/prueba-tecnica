
## Detalle de la Prueba técnica 

Si dispones de tiempo en estos momentos, nos gustaría hacerte una pequeña prueba en PHP para evaluar tu nivel de conocimiento del lenguaje, así como de bases de datos, patrones de diseño, etc. 
Para la prueba habíamos pensado la realización de un pequeño proyecto con un formulario de login, que permitiera el login del usuario y una vez logueado se mostrara un pequeño crud donde se listaran los usuarios que hay en el sistema y cualquier usuario logueado pudiera añadir usuarios, y modificar o eliminar los usuarios ya existentes.
Para esta prueba usaríamos simplemente PHP, JS y CSS, sin otros frameworks o librerías del lenguaje.
En caso de que desees participar, tendrías un plazo de 10 días para realizarla. Muchas gracias por el interés.

### Requisitos para utilizar el proyecto

* PHP 7 o mayor
* Apache
* MySQL

### Pasos para levantar el proyecto

1. Mover el proyecto a un servidor apache
2. Levantar la base de datos que se encuentra en DB/db.sql
3. Configurar los datos de acceso a la base de datos en el archivo lib/ConexionDb.php
4. Ingresar con el usuario **admin:admin**  
    4a. En su defecto crear un nuevo usuario desde la opcion "no tengo usuario"

En caso de no poder levantar el proyecto no duden en contactarme mediante mi corrreo electronico luciano.cassettai@gmail.com

