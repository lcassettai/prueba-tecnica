<?php
declare(strict_types=1);

namespace lib;

class Usuario{
    private $nombre;
    private $apellido;
    private $usuario;
    private $clave;
    private $id;

    public function __construct()
    {
    }

    # GETTERS

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function getUsuario(): ?string
    {
        return $this->usuario;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function getId()
    {
        return $this->id;
    }

    #SETTERS

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;
    }

    public function setApellido(string $apellido)
    {
        $this->apellido = $apellido;
    }

    public function setUsuario(string $usuario)
    {
        $this->usuario = $usuario;
    }

    public function setClave(string $clave)
    {
        $this->clave = $clave;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function load(string $nombre, string $apellido,string $usuario, string $clave){
        $this->setNombre($nombre);
        $this->setApellido($apellido);
        $this->setUsuario($usuario);
        $this->setClave($clave);
    }

    public function loadFromArray(array $usuario){
        if(!empty($usuario['nombre'])){
            $this->setNombre($usuario['nombre']);
        }
        if (!empty($usuario['apellido'])) {
            $this->setApellido($usuario['apellido']);
        }
        if (!empty($usuario['usuario'])) {
            $this->setUsuario($usuario['usuario']);
        }
        if (!empty($usuario['clave'])) {
            $this->setClave($usuario['clave']);
        }
        if(!empty($usuario['id'])){
            $this->setId($usuario['id']);
        }
    }

}
?>
