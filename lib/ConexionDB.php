<?php
namespace lib;

class ConexionDB
{
    private $config = [
        "driver" => "mysql",
        "host" => "localhost",
        "database" => "workanda",
        "port" => "3306",
        "username" => "root",
        "password" => "",
        "charset" => "utf8mb4"
    ];

    public function __construct()
    {
    }

    public function conectar()
    {
        try {
            $driver  = $this->config['driver'];
            $host  = $this->config['host'];
            $database  = $this->config['database'];
            $port  = $this->config['port'];
            $port  = $this->config['port'];
            $username  = $this->config['username'];
            $password  = $this->config['password'];
            $charset  = $this->config['charset'];

            $url = "{$driver}:host={$host}:{$port};dbname={$database};charset={$charset}";

            return new \PDO($url, $username, $password);
        } catch (\PDOException $e) {
            die('La conexion fallo: ' . $e->getMessage());
        }
    }
}
?>
