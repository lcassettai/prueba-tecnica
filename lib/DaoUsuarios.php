<?php
declare(strict_types=1);

namespace lib;

require_once 'lib/ConexionDB.php';
require_once 'lib/Usuario.php';

class DaoUsuarios
{
    private $conexion;
    private $motivoError;
    
    public function __construct(){
        $this->conexion = (new ConexionDB())->conectar();
        $this->motivoError = null;
    }

    public function getMotivoError(){
        return $this->motivoError;
    }


    /**
     * Inserta un nuevo usuario en la base
     */
    public function crear(Usuario $usuario):bool
    {
        if(!$this->usuarioDisponible($usuario->getUsuario())){
            $this->motivoError = "El usuario <strong>{$usuario->getUsuario()}</strong>  ya se encunetra en uso";
            return false;
        }

        $sentencia = $this->conexion->prepare(
            "INSERT INTO usuarios (nombre, apellido, usuario, clave) VALUES (:nombre,:apellido,:usuario, :clave)"
        );

        try {
            $sentencia->bindValue(':nombre', $usuario->getNombre());
            $sentencia->bindValue(':apellido', $usuario->getApellido());
            $sentencia->bindValue(':usuario', $usuario->getUsuario());
            $sentencia->bindValue(':clave', password_hash($usuario->getClave(), PASSWORD_DEFAULT));           

            return $sentencia->execute();
        } catch (\PDOException $e) {
            die('Algo salio mal: ' . $e->getMessage());
        }
    }

    /**
     * Obtiene un usuario a partir de un ID
     */
    public function obtener(int $id){
        $usuario = new Usuario();

        $sentencia = $this->conexion->prepare(
            "SELECT id,nombre,apellido,usuario FROM usuarios WHERE id = :id"
        );
                
        if($sentencia->execute([':id' => $id])){
            $usuario->loadFromArray($sentencia->fetch(\PDO::FETCH_ASSOC));
            return $usuario;
        }else{
            return null;
        }
    }

    /**
     * Actualiza los datos de un usuario existente
     */
    public function actualizar($usuario){
        try {   
            if(empty($usuario->getClave())){
                $sentencia = $this->conexion->prepare(
                    "UPDATE usuarios 
                    SET nombre = :nombre, apellido = :apellido, usuario = :usuario
                    WHERE id = :id"
                        );
            }else{
                $sentencia = $this->conexion->prepare(
                    "UPDATE usuarios 
                    SET nombre = :nombre, apellido = :apellido, usuario = :usuario, clave = :clave
                    WHERE id = :id"
                );
                $sentencia->bindValue(':clave', password_hash($usuario->getClave(), PASSWORD_DEFAULT));
            }

            $sentencia->bindValue(':nombre', $usuario->getNombre());
            $sentencia->bindValue(':apellido', $usuario->getApellido());
            $sentencia->bindValue(':usuario', $usuario->getUsuario());
            $sentencia->bindValue(':id', $usuario->getId());

                    

            return $sentencia->execute();
        } catch (\PDOException $e) {
            die('Algo salio mal: ' . $e->getMessage());
        }
    }

    /**
     * Valida el usuario y clave
     */
    public function validarCredenciales(Usuario $usuario){
        $sentencia = $this->conexion->prepare('SELECT id, usuario, clave FROM usuarios WHERE usuario = :usuario');

        $sentencia->execute([':usuario' => $usuario->getUsuario()]);
        $resultado = $sentencia->fetch(\PDO::FETCH_ASSOC);

        if (!empty($resultado) && password_verify($usuario->getClave(), $resultado['clave'])) {
            return true;
        } else {
            return false;            
        }
    }

    /**
     * Elimina un usuario existente
     */
    public function eliminar(int $id){
        $sentencia = $this->conexion->prepare(
            "DELETE FROM usuarios WHERE id = :id"
        );
        
        return $sentencia->execute([':id' => $id]);
    }

    /**
     * Obtiene los usuarios registrados
     */
    public function obtenerTodos(int $row,int $offset){
        $sentencia = $this->conexion->prepare("SELECT * FROM usuarios LIMIT $row,$offset");
        $sentencia->execute();

        return $sentencia->fetchAll();
    }


    /**
     * Obtiene el total de usuarios registrados (util para la pginacion)
     */
    public function getTotal(){
        $sentencia = $this->conexion->prepare('SELECT count(*) FROM usuarios');
        $sentencia->execute();
        return $sentencia->fetchColumn();
    }

    /**
     * Valida si un usuario esta disponible para su uso
     */
    public function usuarioDisponible($usuario){
        $sentencia = $this->conexion->prepare(
            "SELECT count(usuario) FROM usuarios WHERE usuario = :usuario"
        );

        $sentencia->execute([':usuario' => $usuario]);

        return $sentencia->fetchColumn() <= 0;
    }
}
