<?php
session_start();

if (isset($_SESSION['usuario'])) {
    header('Location: index.php');
    die();
}

use lib\{DaoUsuarios,Usuario};

require_once 'lib/DaoUsuarios.php';
require_once 'lib/Usuario.php';

if (!empty($_POST['usuario']) && !empty($_POST['clave'])) {
    $daoUsuario = new DaoUsuarios();
    $usuario = new Usuario();
    
    $usuario->setUsuario($_POST['usuario']);
    $usuario->setClave($_POST['clave']);

    if($daoUsuario->validarCredenciales($usuario)){
        $_SESSION['usuario'] = $usuario->getUsuario();
        header("Location: index.php");
        die();
    }else{
        $_SESSION['alerta'] = ['error', 'Las crendenciales no coinciden'];
    }   
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <div class="container">
        <div class="form form-login">
            <div class="form-header">
                <img src="assets/image/workanda-logo-big.png" alt="">
                <h1>Iniciar sesión</h1>
                <?php if (!empty($_SESSION['alerta'])) : ?>
                    <div class="alert <?= $_SESSION['alerta'][0] ?>">
                        <?= $_SESSION['alerta'][1] ?>
                    </div>
                    <?php unset($_SESSION['alerta']); ?>
                <?php endif; ?>
            </div>
            <form action="login.php" method="post">
                <div class="input-group">
                    <label for="usuario" class="form-label">Usuario</label>
                    <input type="text" id="usuario" name="usuario" required>
                </div>
                <div class="input-group">
                    <label for="clave" class="form-label">Clave</label>
                    <input type="password" id="clave" name="clave" required>
                </div>
                <div class="form-footer">
                    <button type="submit" class="submit-button">Login</button>
                    <a href="registro.php">No tengo usuario</a>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
