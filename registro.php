<?php
session_start();

use lib\{DaoUsuarios, Usuario};

if (!empty($_SESSION['usuario'])) {
    header('Location: index.php');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require 'lib/Usuario.php';
    require 'lib/DaoUsuarios.php';

    $usuario = new Usuario();
    $usuario->loadFromArray($_POST);

    $daoUsuario = new DaoUsuarios();
    if ($daoUsuario->crear($usuario)) {
        $_SESSION['alerta'] = ['success', 'Se creo el usuario correctamente'];
        header('Location: login.php');
        die();
    } else {
        $_SESSION['alerta'] = ['error', $daoUsuario->getMotivoError()];
    }
}
?>


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <div class="container">
        <div class="form form-registro">
            <div class="form-header">
                <img src="assets/image/workanda-logo-big.png" alt="">
                <h1>Nuevo usuario</h1>
                <?php if (!empty($_SESSION['alerta'])) : ?>
                    <div class="alert <?= $_SESSION['alerta'][0] ?>">
                        <?= $_SESSION['alerta'][1] ?>
                    </div>
                    <?php unset($_SESSION['alerta']); ?>
                <?php endif; ?>
            </div>
            <form action="registro.php" method="post">
                <div class="input-group">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" id="nombre" name="nombre" required>
                </div>
                <div class="input-group">
                    <label for="apellido" class="form-label">Apellido</label>
                    <input type="text" id="apellido" name="apellido" required>
                </div>
                <div class="input-group">
                    <label for="usuario" class="form-label">Usuario</label>
                    <input type="text" id="usuario" name="usuario" required>
                </div>
                <div class="input-group">
                    <label for="clave" class="form-label">Clave</label>
                    <input type="password" id="clave" name="clave" required>
                </div>
                <div class="form-footer">
                    <button type="submit" class="submit-button">Guardar</button>
                    <a href="login.php">Volver al login</a>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
