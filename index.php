<?php
session_start();

use lib\{DaoUsuarios};

if (empty($_SESSION['usuario'])) {
    header('Location: login.php');
    die();
}

require 'lib/DaoUsuarios.php';

$daoUsuarios = new DaoUsuarios();

//Paginacion
$offset = 5;
$entradas = $daoUsuarios->getTotal();
$paginasTotales = ceil($entradas / $offset);
$paginaActual = isset($_GET["pagina"]) ? $_GET["pagina"] : 1;
$row = ($paginaActual - 1) * $offset;

$usuarios = $daoUsuarios->obtenerTodos($row,$offset);
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

    <nav id="menu">
        <img src="assets/image/workanda-logo.png" alt="">

        <div>
            <span><?= $_SESSION['usuario']; ?></span>
            <br>
            <a href="logout.php">Cerrar Sesion</a>
        </div>
    </nav>

    <header>
        <h1>Listado de usuarios</h1>
    </header>

    <section class="container">
        <div class="table">
            <?php if (isset($_SESSION['alerta']) && !is_null($_SESSION['alerta'])) : ?>
                <div class="alert <?= $_SESSION['alerta'][0] ?>">
                    <?= $_SESSION['alerta'][1] ?>
                </div>
                <?php unset($_SESSION['alerta']); ?>
            <?php endif; ?>
            <a href="nuevoUsuario.php" class="submit-button">Nuevo usuario</a>
            <br><br>
            <table id="user-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>NOMBRE</th>
                        <th>APELLIDO</th>
                        <th>USUARIO</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($usuarios)) : ?>
                        <?php foreach ($usuarios as $usuario) : ?>
                            <tr>
                                <td><?= $usuario['id']; ?></td>
                                <td><?= $usuario['nombre']; ?></td>
                                <td><?= $usuario['apellido']; ?></td>
                                <td><?= $usuario['usuario']; ?></td>
                                <td class="table-options">
                                    <a href="editarUsuario.php?ID=<?= $usuario['id']; ?>" title="Editar">
                                        <img class="icon" src="assets/image/pencil-icon.svg" alt="">
                                    </a>
                                    <a class="eliminar" href="eliminarUsuario.php?ID=<?= $usuario['id']; ?>" title="Eliminar">
                                        <img class="icon" src="assets/image/trash-icon.svg" alt="">
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
            <?php if ($paginasTotales > 1) : ?>
                <div class="paginacion">
                    <?php if ($paginaActual != 1) : ?>
                        <a href="index.php?pagina=<?php echo $paginaActual - 1 ?>">
                            <</a>
                            <?php endif; ?>
                            <?php
                            for ($i = 1; $i <= $paginasTotales; $i++) {
                                if($i == $paginaActual){
                                    printf("<a id='pagina-actual' href='index.php?pagina=%u'>%u</a>", $i, $i);
                                }else{
                                    printf("<a href='index.php?pagina=%u'>%u</a>", $i, $i);
                                }
                            }
                            ?>
                            <?php if ($paginaActual < $paginasTotales) : ?>
                                <a href="index.php?pagina=<?php echo $paginaActual + 1 ?>">></a>
                            <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </section>

    <footer>
        <p>
            Desarrollado por <a target="_blank" href="https://www.linkedin.com/in/luciano-cassettai-88a038a6/">Luciano Cassettai</a>
        </p>
    </footer>
    <script src="assets/js/scripts.js"></script>
</body>

</html>
