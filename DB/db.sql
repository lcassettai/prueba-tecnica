START TRANSACTION;

CREATE DATABASE IF NOT EXISTS `workanda` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `workanda`;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `apellido` varchar(200) NOT NULL,
  `usuario` varchar(200) NOT NULL,
  `clave` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuario` (`usuario`);


ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;


INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `usuario`, `clave`) VALUES
(1, 'Luciano', 'Cassettai', 'admin', '$2y$10$i7G69qvULpPJjGYv5fGHIunANQyGwzI9OqH56FWqVo7iT7SeY1bji');



