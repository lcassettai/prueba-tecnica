<?php
session_start();

use lib\{DaoUsuarios};

if (!isset($_SESSION['usuario'])) {
    header('Location: login.php');
    die();
}

require 'lib/DaoUsuarios.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['ID'])) {
    $daoUsuarios = new DaoUsuarios();

    if ($daoUsuarios->eliminar($_GET['ID'])) {
        $_SESSION['alerta'] = ['success', 'Se eliminó el usuario correctamente'];
        header('Location: index.php');
    } else {
        $_SESSION['alerta'] = ['error', 'No se pudo eliminar al usuario'];
        header('Location: index.php');
    } 
}
?>
