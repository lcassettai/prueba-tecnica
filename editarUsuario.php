<?php
session_start();

use lib\{DaoUsuarios, Usuario};

if (empty($_SESSION['usuario'])) {
    header('Location: login.php');
    die();
}

require 'lib\Usuario.php';
require 'lib/DaoUsuarios.php';

$daoUsuario = new DaoUsuarios();

if ($_SERVER['REQUEST_METHOD'] === 'GET' && !empty($_GET['ID'])) {
    $usuario = $daoUsuario->obtener($_GET['ID']);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $usuarioActualizado = new Usuario();
    $usuarioActualizado->loadFromArray($_POST);

    if ($daoUsuario->actualizar($usuarioActualizado)) {
        $_SESSION['alerta'] = ['success', 'Se editó el usuario correctamente'];
        header('Location: index.php');
        die();
    } else {
        return false;
    }
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <nav id="menu">
        <img src="assets/image/workanda-logo.png" alt="">
        <div>
            <span><?= $_SESSION['usuario']; ?></span>
            <br>
            <a href="logout.php">Cerrar Sesion</a>
        </div>
    </nav>

    <div class="container">
        <div class="form">
            <div class="form-header">
                <h1>Editar usuario</h1>
                <?php if (isset($_SESSION['alerta']) && !is_null($_SESSION['alerta'])) : ?>
                    <div class="alert <?= $_SESSION['alerta'][0] ?>">
                        <?= $_SESSION['alerta'][1] ?>
                    </div>
                <?php endif; ?>
            </div>
            <form action="editarUsuario.php" method="post">
                <input type="hidden" id="id" name="id" required value="<?= $usuario->getId(); ?>">
                <div class="input-group">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" id="nombre" name="nombre" required value="<?= $usuario->getNombre(); ?>">
                </div>
                <div class="input-group">
                    <label for="apellido" class="form-label">Apellido</label>
                    <input type="text" id="apellido" name="apellido" required value="<?= $usuario->getApellido(); ?>">
                </div>
                <div class=" input-group">
                    <label for="usuario" class="form-label">Usuario</label>
                    <input type="text" id="usuario" name="usuario" required value="<?= $usuario->getUsuario(); ?>">
                </div>
                <div class="input-group">
                    <label for="clave" class="form-label">Clave</label>
                    <input type="password" id="clave" name="clave">
                </div>
                <div class="form-footer">
                    <button type="submit" class="submit-button">Guardar</button>
                    <a href="index.php">Cancelar</a>
                </div>
            </form>
        </div>
    </div>

    <footer>
        <p>
            Desarrollado por <a target="_blank" href="https://www.linkedin.com/in/luciano-cassettai-88a038a6/">Luciano Cassettai</a>
        </p>
    </footer>
</body>

</html>
